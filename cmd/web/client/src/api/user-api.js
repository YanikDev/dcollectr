import httpClient from './http.js'

export default {

  createAccount(obj) {
    return httpClient.request('post', 'users', obj);
  },
}