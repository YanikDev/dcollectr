import axios from 'axios';

const client = axios.create({
  baseURL: process.env.VUE_APP_ROOT_API,
  json: true
})

export default {
  async request(method, url, data, headers = {}) {
    let accessToken = localStorage.getItem('token') || null;

    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`
    }

    try {
      let req = await client({
        method,
        url: url,
        data: data,
        headers: headers,
        timeout: 6000000
      })

      return req.data;
    } catch (err) {

      if (err.response && err.response.status === 440) {
        // try {
        //   let res = await AuthService.refreshToken();
        //   localStorage.setItem('token', res.data.token);
        //   this.request(method, url, data, headers)
        //   window.location.reload();
        //   return;
        // } catch(e) {
        //   err = e;
        // }
      }
    
      if (err.response && (err.response.status === 403)) {
        window.location.href = window.location.origin + "/logout";
        return;
      }

      throw err
    }
    
  },
}
