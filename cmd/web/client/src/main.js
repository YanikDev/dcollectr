import Vue from 'vue'
import App from './App.vue'
import { ValidationProvider, extend } from 'vee-validate';
import { email, required, alpha_spaces, max, min } from 'vee-validate/dist/rules';
import router from './routes/router'
import '@/assets/css/tailwind.css';


// Validation Rules
// extend('secret', {
//   validate: value => value === 'example',
//   message: 'This is not the magic word'
// });
// No message specified.

extend('min', min)
extend('max', max)
extend('alpha_spaces', alpha_spaces)
extend('email', {
  ...email,
  message: 'This field requires a valid email'
});

extend('required', {
  ...required,
  message: 'This field is required'
});
// Register it globally
Vue.component('ValidationProvider', ValidationProvider);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
