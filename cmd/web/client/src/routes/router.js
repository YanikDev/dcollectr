import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)


const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      name: 'home',
      component: () => import(/* webpackChunkName: "login" */ '@/pages/Home.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ '@/pages/Login.vue'),
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: () => import(/* webpackChunkName: "login" */ '@/pages/SignUp.vue'),
    },
    // {
    //   path: '',
    //   component: () => import(/* webpackChunkName: "login" */ '@/views/Index.vue'),
    //   children: [
    //     {
    //       path: '/',
    //       name: '',
    //       component: () => import(/* webpackChunkName: "login" */ '@/layouts/Layout.vue'),
    //       redirect: {
    //         name: "login"
    //       },
    //       children: [
    //        {
    //          path:'login',
    //          name: 'login',
    //          component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue'),
    //        },
    //       ],
    //     },        
    //   ]
    // },
    
  ]
})

router.beforeEach((to, from, next) => {
  // let authRequired = true;
  // const publicPages = ['/login', '/setup', '/logout', 'forgot/password'];
  // const loggedIn = localStorage.getItem('user');
  // const token = localStorage.getItem('token');
  // const refreshToken = localStorage.getItem('refresh_token');

  // //redirect to app modules if user is already logged in
  // if(to.path == '/login' && loggedIn) {
  //   next('/app');
  //   return;
  // }

  // for(const el of publicPages) {
  //   if(to.path.indexOf(el) > -1) {
  //     authRequired = false
  //     break;
  //   }
  // }
  
  // //redirect to login page if not logged in and trying to access a restricted page
  // if (authRequired && !loggedIn) {
  //   return next('/login');
  // }

  // if(authRequired && loggedIn) {
  //   // let now = new Date()
  //   // let decodedRefreshToken = jwtDecoder(refreshToken)
  //   // //todo: show message to user that he/she will be logged out within the next 1 min
  //   // //todo: explore using sockets to tell clients that they have a 3mins left from each api
  //   // if ((now.getTime()/1000) > decodedRefreshToken.exp) {
  //   //   return next('/logout');
  //   // }  
  // }

  next();
})

export default router