package web

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/devprojx/dcollectr/util"
	// "github.com/gobuffalo/packr/packr"
	"github.com/gorilla/mux"
)

//Page defines the struct of a webpage
type Page struct {
	Title string
	Js    []string
	CSS   []string
}

var tmpl *template.Template
var workDir string
var templatesPath string
var page Page

func init() {
	workDir, err := os.Getwd()
	if err != nil {
		log.Printf("[error] unable to get working directory: %s", err)
	}

	templatesPath = "./cmd/web/templates/"

	log.Print("[info] templates loading...")
	tmpl, err = template.ParseGlob(filepath.Join(workDir, templatesPath+"*"))
	if err != nil {
		log.Printf("[error] unable to load tempalates: %s", err)
	}

	_ = tmpl
}

//StartAPIServer starts api server to server the listings
func StartAPIServer(cfg *util.Config) {
	router := mux.NewRouter()

	webRoutes(router)

	log.Print("[info] initializing router")

	srv := &http.Server{
		Handler:      router,
		Addr:         cfg.Server.Web.Host + ":" + cfg.Server.Web.Port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		log.Fatal(srv.ListenAndServe())
	}()

	log.Print("[info] server running at " + srv.Addr)
}

func webRoutes(r *mux.Router) {
	fs := http.FileServer(http.Dir("./cmd/web/public/"))
	r.PathPrefix("/public/").Handler(http.StripPrefix("/public/", fs))
}
