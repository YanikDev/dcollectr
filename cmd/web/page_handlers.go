package web

import (
	"log"
	"net/http"
)

type loginPage struct {
	Page
}

func HomePageHandler(w http.ResponseWriter, r *http.Request) {
	data := loginPage{Page: page}
	err := tmpl.ExecuteTemplate(w, "home", data)
	if err != nil {
		log.Println("[error]: loading home template ", err)
	}
}
