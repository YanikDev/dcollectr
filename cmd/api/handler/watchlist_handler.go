package handler

import (
	"context"
	"net/http"
	"strconv"
	"strings"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/repository"
	"github.com/gofiber/fiber/v2"
)

type ListHandler struct {
	Repository repository.ListRepo
}

type singleRequest struct {
	model.SearchRequest
	Format       string   `json:"format"`
	APIKey       string   `json:"api_key"`
	MinimumScore []string `json:"min_score"`
}

type batchRequest struct {
	SearchParams []model.SearchRequest `json:"search_params"`
	Format       string                `json:"format"`
	APIKey       string                `json:"api_key"`
	MinimumScore []string              `json:"min_score"`
}

//SingleSearchHandler handles a single search requests
func (h *ListHandler) SingleSearchHandler(c *fiber.Ctx) error {
	var req singleRequest
	var searchFields model.SearchRequest

	if c.Route().Method == "GET" {
		sources := c.Query("sources")
		names := c.Query("names")
		dataSets := c.Query("datasets")
		types := c.Query("types")
		searchAlias := c.Query("search_alias")
		fuzzySearch := c.Query("use_fuzzy")

		searchFields.Names = strings.Split(names, ",")
		searchFields.Sources = strings.Split(sources, ",")
		searchFields.DataSets = strings.Split(dataSets, ",")
		searchFields.Types = strings.Split(types, ",")

		searchFields.SearchAlias = false

		includeAlias, err := strconv.ParseBool(searchAlias)
		if err == nil {
			searchFields.SearchAlias = includeAlias
		}

		useFuzzySearch, err := strconv.ParseBool(fuzzySearch)
		if err == nil {
			searchFields.UseFuzzy = useFuzzySearch
		}

		req.SearchRequest = searchFields
	} else if c.Route().Method == "POST" {
		err := c.BodyParser(&req)
		if err != nil {
			return fiber.NewError(fiber.ErrUnprocessableEntity.Code, "unable to decode body")
		}
	}

	ctx := context.TODO()
	result := h.Repository.SingleSearch(ctx, req.SearchRequest)

	return c.Status(http.StatusOK).JSON(result)

}

//BatchHandler handles batch search requests
func (h *ListHandler) BatchHandler(c *fiber.Ctx) error {

	var req batchRequest
	err := c.BodyParser(&req)

	if err != nil {
		return fiber.NewError(fiber.ErrUnprocessableEntity.Code, "unable to decode body")
	}

	ctx := context.TODO()

	result := h.Repository.BulkSearch(ctx, req.SearchParams)

	return c.Status(http.StatusOK).JSON(result)
}
