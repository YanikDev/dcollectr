package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/repository"
	"github.com/devprojx/dcollectr/util"
)

type UserHandler struct {
	Repository repository.UserRepo
	Cfg        *util.Config
}

func (h *UserHandler) SignUpHandler(w http.ResponseWriter, r *http.Request) {
	var newUser model.User
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		log.Println("[error]: error decoding body ", err)
	}

	user := h.Repository.FindUserByEmail(context.TODO(), newUser.Email)
	if err != nil {
		util.WriteHTTPResponse(w, http.StatusConflict, "json", nil)
		return
	}

	newUser.APIKeys = []model.APIKey{
		{
			Name:      "",
			Prefix:    "",
			KeyHash:   " ",
			RateLimit: 0,
		},
	}

	h.Repository.InsertUser(context.TODO(), newUser)
	email, err := util.HTMLTemplateToString("./templates/registration-template.html", user)
	if err != nil {
		log.Println("[error]: parsing email template ", err)

	}

	if _, err := util.SendEmail(*h.Cfg, []string{newUser.Email}, []string{}, "Account Created", email, "", ""); err != nil {
		print(err.Error())
		return
	}
}
