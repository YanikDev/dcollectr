package api

import (
	"log"
	"strings"

	"github.com/devprojx/dcollectr/cmd/api/handler"
	"github.com/devprojx/dcollectr/cmd/api/router"
	"github.com/devprojx/dcollectr/repository"
	"github.com/devprojx/dcollectr/repository/list"
	"github.com/devprojx/dcollectr/util"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

//StartAPIServer starts api server to server the listings
func StartAPIServer(cfg *util.Config) {
	//Init logger
	logger := util.NewLogger()

	//Init database clients
	var listRepo repository.ListRepo
	elasticSearchClient, err := util.InitElasticSearchDB(cfg.DB.ElasticSearch.URLS, cfg.DB.ElasticSearch.Username, cfg.DB.ElasticSearch.Password)
	if err != nil {
		mongoClient, err := util.InitMongoDB(cfg.DB.Mongo.URI)
		if err != nil {
			logger.Warnf("[error] unable to connect to elasticsearch or mongo database: %s", err)
		}

		listRepo = list.NewMongoListRepo(mongoClient)
	}

	listRepo = list.NewElasticSearchRepo(elasticSearchClient, logger)
	listHandler := &handler.ListHandler{
		Repository: listRepo,
	}

	logger.Info("initializing router...")

	app := fiber.New(fiber.Config{
		BodyLimit: 20 * 1024 * 1024,
	})

	corsOptions := cors.Config{
		Next: nil,
		// AllowOrigins: strings.Join(cfg.Server.AllowedOrigins, ", "),
		AllowMethods: strings.Join([]string{
			fiber.MethodGet,
			fiber.MethodPost,
			fiber.MethodHead,
			fiber.MethodPut,
			fiber.MethodDelete,
			fiber.MethodPatch,
			fiber.MethodOptions,
		}, ","),
		AllowCredentials: false,
		ExposeHeaders:    "",
		MaxAge:           0,
	}

	//Middlewares
	app.Use(cors.New(corsOptions))
	router.SetupWatchlistRoutes(app, listHandler)

	go func() {
		log.Fatal(app.Listen(cfg.EmailClient.Host + ":" + cfg.Server.API.Port))
	}()
}
