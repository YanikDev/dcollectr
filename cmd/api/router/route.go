package router

import (
	"github.com/devprojx/dcollectr/cmd/api/handler"
	"github.com/gofiber/fiber/v2"
)

//SetupWatchlistRoutes func
func SetupWatchlistRoutes(app *fiber.App, handler *handler.ListHandler) {
	//Middleware
	api := app.Group("/")

	//Routes
	api.Get("/", handler.SingleSearchHandler).
		Post("/", handler.SingleSearchHandler)
	api.Post("/batch", handler.BatchHandler)
}
