package cli

import (
	"context"

	"github.com/devprojx/dcollectr/lib"
	"github.com/devprojx/dcollectr/lib/criminals/fbi"
	"github.com/devprojx/dcollectr/lib/peps/jmgov"
	"github.com/devprojx/dcollectr/lib/sanctions/bis"
	"github.com/devprojx/dcollectr/lib/sanctions/sdn"
	"github.com/devprojx/dcollectr/lib/sanctions/unlist"
	"github.com/devprojx/dcollectr/lib/sanctions/wrldbk"

	"github.com/devprojx/dcollectr/repository/list"
	"github.com/devprojx/dcollectr/util"
)

//StartWebScrapingProcess runs webscrapping process in the background
func StartWebScrapingProcess(cfg *util.Config) {
	//Init logger
	logger := util.NewLogger()

	logger.Info("process is running in the background... ")
	// mongoClient := util.InitMongoDB(cfg.DB.Mongo.URI)
	// listRepo := list.NewMongoListRepo(mongoClient)

	esClient, err := util.InitElasticSearchDB(cfg.DB.ElasticSearch.URLS, cfg.DB.ElasticSearch.Username, cfg.DB.ElasticSearch.Password)
	if err != nil {
		logger.Fatalf("unable to connect to elasticsearch database: %s", err)
	}
	listRepo := list.NewElasticSearchRepo(esClient, logger)

	scraper := lib.Scraper{
		Repository: listRepo,
		Scrapers:   []lib.ScraperFunc{jmgov.ScrapeData, fbi.ScrapeData, bis.ScrapeData, wrldbk.ScrapeData, sdn.ScrapeData, unlist.ScrapeData},
	}

	ctx := context.TODO()
	scraper.Process(ctx)
}
