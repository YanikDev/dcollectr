package fbi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/util"
	"github.com/mitchellh/mapstructure"
)

func ScrapeData() []model.FormattedData {
	var formattedData []model.FormattedData

	parsedData := Parse()
	for _, data := range parsedData {
		var metaData map[string]interface{}

		err := mapstructure.Decode(data, &metaData)
		if util.IsError(err) {
			return nil
		}

		fd := model.FormattedData{
			Name:        data.Title,
			Aliases:     strings.Join(data.Aliases, ", "),
			SourceName:  "THE FEDERAL BUREAU OF INVESTIGATION",
			SourceCode:  "FBI",
			DataSet:     "criminal",
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			Type:        model.INDIVIDUAL,
			MetaData:    metaData,
		}

		formattedData = append(formattedData, fd)
	}
	return formattedData
}

func Parse() []FBI {
	page := 1
	totalPages := 0
	var list []FBI

	for {
		res, err := util.GetContent("https://api.fbi.gov/@wanted?page="+strconv.Itoa(page)+"&pageSize=20", nil)
		if util.IsError(err) {
			return nil
		}
		defer res.Body.Close()

		jsn, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Print(err)
			return nil
		}

		var result struct {
			Total    int32 `json:"total"`
			Listings []FBI `json:"items"`
		}

		err = json.Unmarshal(jsn, &result)
		if util.IsError(err) {
			return nil
		}

		for _, el := range result.Listings {
			list = append(list, el)
		}
		totalPages = int(math.Round(float64(result.Total / 20)))

		if totalPages <= page {
			break
		}

		page++
	}

	return list
}
