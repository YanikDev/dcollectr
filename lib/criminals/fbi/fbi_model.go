package fbi

type FBI struct {
	ID          string `json:"uid" mapstructure:"id"`
	Title       string `json:"title" mapstructure:"title"`
	Description string `json:"description" mapstructure:"description"`
	Images      []struct {
		Caption  string `json:"caption" mapstructure:"caption"`
		Original string `json:"original" mapstructure:"original"`
		Large    string `json:"large" mapstructure:"large"`
		Thumb    string `json:"thumb" mapstructure:"thumb"`
	} `json:"images" mapstructure:"images"`
	Files []struct {
		Url  string `json:"url" mapstructure:"url"`
		Name string `json:"name" mapstructure:"name"`
	} `json:"files" mapstructure:"files"`
	WarningMessage        string   `json:"warning_message" mapstructure:"warning_message"`
	Caution               string   `json:"caution" mapstructure:"caution"`
	Remarks               string   `json:"remarks" mapstructure:"remarks"`
	Details               string   `json:"details" mapstructure:"details"`
	AdditionalInformation string   `json:"additional_information" mapstructure:"additional_information"`
	RewardText            string   `json:"reward_text" mapstructure:"reward_text"`
	RewardMin             int32    `json:"reward_min" mapstructure:"reward_min"`
	RewardMax             int32    `json:"reward_max" mapstructure:"reward_max"`
	Names                 []string `json:"legat_names" mapstructure:"names"`
	Aliases               []string `json:"aliases" mapstructure:"aliases"`
	FieldOffices          []string `json:"field_offices" mapstructure:"field_offices"`
	Locations             []string `json:"locations" mapstructure:"locations"`
	PlaceOfBirth          string   `json:"place_of_birth" mapstructure:"place_of_birth"`
	Status                string   `json:"status" mapstructure:"status"` //F means entity and I means individuals
	PersonClassification  string   `json:"person_classification" mapstructure:"person_classification"`
	Ncic                  string   `json:"ncic" mapstructure:"ncic"`
	AgeMin                int32    `json:"age_min" mapstructure:"age_min"`
	AgeMax                int32    `json:"age_max" mapstructure:"age_max"`
	WeightMin             int32    `json:"weight_min" mapstructure:"weight_min"`
	WeightMax             int32    `json:"weight_max" mapstructure:"weight_max"`
	HeightMin             int32    `json:"height_min" mapstructure:"height_min"`
	HeightMax             int32    `json:"height_max" mapstructure:"height_max"`
	Eyes                  string   `json:"eyes" mapstructure:"eyes"`
	Hair                  string   `json:"hair" mapstructure:"hair"`
	Build                 string   `json:"build" mapstructure:"build"`
	Sex                   string   `json:"sex" mapstructure:"sex"`
	Race                  string   `json:"race" mapstructure:"race"`
	Nationality           string   `json:"nationality" mapstructure:"nationality"`
	ScarsAndMarks         string   `json:"scars_and_marks" mapstructure:"scars_and_marks"`
	Complexion            string   `json:"complexion" mapstructure:"complexion"`
	Occupations           []string `json:"occupations" mapstructure:"occupations"`
	PossibleCountries     []string `json:"possible_countries" mapstructure:"possible_countries"`
	PossibleStates        []string `json:"possible_states" mapstructure:"possible_states"`
	Publication           string   `json:"publication" mapstructure:"publication"`
	Path                  string   `json:"Path" mapstructure:"Path"`
}
