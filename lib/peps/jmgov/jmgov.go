package jmgov

import (
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/util"
	"github.com/mitchellh/mapstructure"
)

type Data struct {
	ImageURL          string `json:"url"`
	Position          string `json:"position"`
	Name              string `json:"name"`
	Constituency      string
	ElectorateDivsion string
	Description       string
	Party             string
	PoliticianType    string
}

func ScrapeData() []model.FormattedData {

	parsedData := FindGovAndOpposition()
	parsedData = append(parsedData, FindCouncillors()...)

	var formattedData []model.FormattedData
	for _, data := range parsedData {
		var metaData map[string]interface{}

		err := mapstructure.Decode(data, &metaData)
		if util.IsError(err) {
			return nil
		}

		fd := model.FormattedData{
			Name:        data.Name,
			SourceName:  "JAMAICA GOVERNMENT",
			SourceCode:  "JMGOV",
			DataSet:     "PEP",
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			Type:        model.INDIVIDUAL,
			MetaData:    metaData,
		}

		formattedData = append(formattedData, fd)
	}
	return formattedData
}

func FindCouncillors() []Data {
	res, err := util.GetContent("https://www.localgovjamaica.gov.jm/councillors/", nil)
	if util.IsError(err) {
		return nil
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	tot := "0"
	f := strings.ReplaceAll(doc.Find(".current-page").Text(), "\n", " ")
	arr := strings.Split(strings.TrimSpace(f), " ")
	if len(arr) == 4 {
		tot = arr[3]
	}

	totalPage, err := strconv.ParseInt(tot, 0, 64)
	if err != nil {
		totalPage = int64(0)
	}

	getPageFunc := func(page int64) []Data {
		res, err := util.GetContent("https://www.localgovjamaica.gov.jm/councillors/?sf_paged="+strconv.FormatInt(page, 10), nil)
		if util.IsError(err) {
			return nil
		}
		defer res.Body.Close()

		doc, err := goquery.NewDocumentFromReader(res.Body)

		if err != nil {
			log.Fatal(err)
		}

		var data []Data
		doc.Find(".search-filter-results.table .row").Each(func(i int, r *goquery.Selection) {
			var d Data
			if !r.HasClass("head") {
				name := strings.TrimSpace(r.Find("div:nth-of-type(2)").Text())
				party := strings.TrimSpace(r.Find("div:nth-of-type(3)").Text())
				constituency := strings.TrimSpace(r.Find("div:nth-of-type(4)").Text())
				division := strings.TrimSpace(r.Find("div:nth-of-type(5)").Text())

				d.Name = strings.ToUpper(name)
				d.Constituency = constituency
				d.ElectorateDivsion = division
				d.Party = party
				data = append(data, d)
			}

		})

		return data
	}

	var i int64
	ch := make(chan []Data, 1)

	for i = 0; i < totalPage; i++ {
		page := i
		go func() {
			ch <- getPageFunc(page)
		}()

	}

	var listing []Data
	var count int64
	for c := range ch {
		count++
		listing = append(listing, c...)
		if count == i {
			close(ch)
		}
	}

	return listing
}

func FindGovAndOpposition() []Data {
	res, err := util.GetContent("https://jis.gov.jm/government/members-of-parliament", nil)
	if util.IsError(err) {
		return nil
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var data []Data
	doc.Find("table").Each(func(i int, table *goquery.Selection) {
		var d Data
		d.Party = "Government"
		if i == 1 {
			d.Party = "Oposition"
		}
		table.Find("tbody tr").Each(func(i int, s *goquery.Selection) {

			name := s.Find("td:nth-of-type(1)").Text()
			constituency := s.Find("td:nth-of-type(2)").Text()

			d.Name = strings.TrimSpace(strings.ToUpper(name))
			d.Constituency = strings.TrimSpace(constituency)
			data = append(data, d)
		})

	})

	return data
}
