package lib

import (
	"context"
	// "log"
	// "time"

	"github.com/devprojx/dcollectr/model"
)

type ScraperFunc func() []model.FormattedData

type Scraper struct {
	Repository interface {
		Insert(ctx context.Context, records []interface{})
	}
	Scrapers []ScraperFunc
}

//Process runs a list of giver web scraper
func (s *Scraper) Process(ctx context.Context) {
	ch := make(chan []model.FormattedData)

	for _, scraper := range s.Scrapers {
		fn := scraper
		go func() {
			ch <- fn()
		}()
	}

	//write each data to the database
	go func() {
		for c := range ch {
			var records []interface{}
			for _, x := range c {
				records = append(records, x)
			}

			s.Repository.Insert(ctx, records)
		}
	}()
}
