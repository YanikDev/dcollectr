package bis

import (
	"encoding/csv"
	"log"
	"time"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/util"
)

func ScrapeData() []model.FormattedData {
	var formattedData []model.FormattedData

	parsedData := Parse()
	if parsedData == nil {
		log.Println("[error] unable to fetch and parse BUREAU OF INDUSTRY AND SECURITY documents  totalling")
		return nil
	}

	for _, data := range parsedData {
		metaData, err := util.JSONToMap(data)
		if util.IsError(err) {
			return nil
		}

		fd := model.FormattedData{
			Name:        data.Name,
			SourceName:  "BUREAU OF INDUSTRY AND SECURITY",
			SourceCode:  "BIS",
			DataSet:     "sanction",
			Type:        model.UNKNOWN,
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			MetaData:    metaData,
		}

		formattedData = append(formattedData, fd)
	}
	return formattedData
}

func Parse() []BISDenied {
	res, err := util.GetContent("https://www.bis.doc.gov/dpl/dpl.txt", nil)
	if util.IsError(err) {
		return nil
	}
	defer res.Body.Close()

	r := csv.NewReader(res.Body)
	r.Comma = '\t'
	r.Comment = '#'

	lines, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	var denied []BISDenied
	for idx, line := range lines {
		if idx == 0 {
			continue
		}

		var bis BISDenied
		var address Address

		bis.Name = line[0]
		address.Street = line[1]
		address.City = line[2]
		address.Province = line[3]
		address.Country = line[4]
		address.PostalCode = line[5]
		bis.DateEffective = line[6]
		bis.ExpirationDate = line[7]
		bis.StandardOrder = line[8]
		bis.LastUpdated = line[9]
		bis.Action = line[10]
		bis.FRCitation = line[11]

		bis.Address = &address

		denied = append(denied, bis)
	}

	return denied
}
