package bis

type Address struct {
	Street     string `json:"street" xml:"street"`
	City       string `json:"city" xml:"city"`
	Province   string `json:"province" xml:"province"`
	Country    string `json:"country" xml:"country"`
	PostalCode string `json:"postal_code" xml:"postal_code"`
}

type BISDenied struct {
	Name           string   `json:"name" xml:"name"`
	Address        *Address `json:"address" xml:"address"`
	DateEffective  string   `json:"date_effective" xml:"date_effective"`
	ExpirationDate string   `json:"expiration_date" xml:"expiration_date"`
	StandardOrder  string   `json:"standard_order" xml:"standard_order"`
	LastUpdated    string   `json:"last_updated" xml:"last_updated"`
	Action         string   `json:"action" xml:"action"`
	FRCitation     string   `json:"fr_citation" xml:"fr_citation"`
}
