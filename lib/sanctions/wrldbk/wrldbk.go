package wrldbk

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/util"
	"github.com/mitchellh/mapstructure"
)

func ScrapeData() []model.FormattedData {
	var formattedData []model.FormattedData

	parsedData := Parse()
	if parsedData == nil {
		log.Println("[error] unable to fetch and parse WORLDBANK DEBARRED PROVIDERS documents")
		return nil
	}

	for _, data := range parsedData {
		var metaData map[string]interface{}

		err := mapstructure.Decode(data, &metaData)
		if util.IsError(err) {
			return nil
		}

		fd := model.FormattedData{
			Name:        data.Name,
			SourceName:  "WORLDBANK DEBARRED PROVIDERS",
			SourceCode:  "WBP",
			DataSet:     "sanction",
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			Type:        model.ENTITY,
			MetaData:    metaData,
		}

		if data.SupplierTypeCode == "I" {
			fd.Type = model.INDIVIDUAL
		}

		formattedData = append(formattedData, fd)
	}
	return formattedData
}

func Parse() []WorldBankDeBarSupplier {
	var hdrs []map[string]string

	m := make(map[string]string)
	hdrs = append(hdrs, m)
	m["apikey"] = "z9duUaFUiEUYSHs97CU38fcZO7ipOPvm"
	res, err := util.GetContent("https://apigwext.worldbank.org/dvsvc/v1.0/json/APPLICATION/ADOBE_EXPRNCE_MGR/FIRM/SANCTIONED_FIRM", hdrs)
	if util.IsError(err) {
		return nil
	}
	defer res.Body.Close()

	jsn, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
		return nil
	}

	var result struct {
		Response struct {
			Listings []WorldBankDeBarSupplier `json:"ZPROCSUPP"`
		} `json:"response"`
	}

	err = json.Unmarshal(jsn, &result)
	if util.IsError(err) {
		return nil
	}

	return result.Response.Listings
}
