package wrldbk

type WorldBankDeBarSupplier struct {
	ID                  int32  `json:"SUPP_ID" mapstructure:"supplier_id"`
	Name                string `json:"SUPP_NAME" mapstructure:"name"`
	SupplierTypeCode    string `json:"SUPP_TYPE_CODE" mapstructure:"supplier_type_code"` //F means entity and I means individuals
	Street              string `json:"SUPP_ADDR" mapstructure:"street"`
	City                string `json:"SUPP_CITY" mapstructure:"city"`
	Province            string `json:"SUPP_PROV" mapstructure:"province"`
	Country             string `json:"COUNTRY_NAME" mapstructure:"country"`
	DebarReason         string `json:"DEBAR_REASON" mapstructure:"debarred_reason"`
	DebarFrom           string `json:"DEBAR_FROM_DATE" mapstructure:"debar_from_date"`
	DebarTo             string `json:"DEBAR_TO_DATE" mapstructure:"debar_to_date"`
	IneligibilityStatus string `json:"INELIGIBLY_STATUS" mapstructure:"ineligibility_status"`
}
