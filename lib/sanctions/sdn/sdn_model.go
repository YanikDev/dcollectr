package sdn

import (
	"encoding/xml"
)

//SDN defines the SDN listing xml structure
type SDN struct {
	XMLName         xml.Name           `xml:"sdnList"`
	ConolidatedData []ConsolidatedData `xml:"sdnEntry"`
}

//Address defines an address
type Address struct {
	Address1   string `xml:"address1" json:"address1"`
	Address2   string `xml:"address2" json:"address2"`
	Address3   string `xml:"address3" json:"address3"`
	City       string `xml:"city" json:"city"`
	Province   string `xml:"stateOrProvince" json:"state_or_province"`
	Country    string `xml:"country" json:"country"`
	PostalCode string `xml:"postalCode" json:"postal_code"`
}

//Alias defines an enity or indviduals alias
type Alias struct {
	Type      string `xml:"type" json:"type"`
	Category  string `xml:"category" json:"category"`
	FirstName string `xml:"firstName" json:"first_name"`
	LastName  string `xml:"lastName" json:"last_name"`
}

//VesselInformation defines additional information about a vessel
type VesselInformation struct {
	CallSign               string `xml:"callSign" json:"call_sign"`
	VesselType             string `xml:"vesselType" json:"vessel_type"`
	VesselFlag             string `xml:"vesselFlag" json:"vessel_flag"`
	VesselOwner            string `xml:"vesselOwner" json:"vessel_owner"`
	Tonnage                string `xml:"tonnage" json:"tonnage"`
	GrossRegisteredTonnage string `xml:"grossRegisteredTonnage" json:"gross_registered_tonnage"`
}

//ConsolidatedData defines an individual or entity on the SDN list
type ConsolidatedData struct {
	DataID       string             `xml:"uid" json:"data_id"`
	Remarks      string             `xml:"remarks" json:"remarks"`
	ProgramList  []string           `xml:"programList>program" json:"program_list"`
	SDNType      string             `xml:"sdnType" json:"sdn_type"`
	FirstName    string             `xml:"firstName" json:"first_name"`
	LastName     string             `xml:"lastName" json:"last_name"`
	Aliases      []Alias            `xml:"akaList>aka" json:"aliases"`
	DateOfBirth  string             `xml:"dateOfBirthList>dateOfBirthItem>dateOfBirth" json:"date_of_birth"`
	PlaceOfBirth string             `xml:"placeOfBirthList>placeOfBirthItem>placeOfBirth" json:"place_of_birth"`
	Addresses    []Address          `xml:"addressList>address" json:"addresses"`
	VesselInfo   *VesselInformation `xml:"vesselinfo" json:"vessel_info"`
}
