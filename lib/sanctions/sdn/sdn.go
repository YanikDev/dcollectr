package sdn

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/util"
)

func ScrapeData() []model.FormattedData {
	var formattedData []model.FormattedData

	parsedData := FetchAndParseData()
	if parsedData == nil {
		log.Println("[error] unable to fetch and parse SPECIALLY DESIGNATED NATIONALS documents")
		return nil
	}

	for _, data := range parsedData.ConolidatedData {

		metaData, err := util.JSONToMap(data)
		if util.IsError(err) {
			return nil
		}

		var aliases string
		for _, a := range data.Aliases {
			aliases += a.FirstName + " " + a.LastName + " "
		}

		fd := model.FormattedData{
			Name:        data.FirstName + " " + data.LastName,
			SourceName:  "SPECIALLY DESIGNATED NATIONALS",
			SourceCode:  "OFAC",
			DataSet:     "sanction",
			Aliases:     aliases,
			Type:        model.INDIVIDUAL,
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			MetaData:    metaData,
		}

		if data.SDNType == "Entity" {
			fd.Type = model.ENTITY
		} else if data.SDNType == "Vessel" {
			fd.Type = model.VESSEL
		}

		formattedData = append(formattedData, fd)
	}

	return formattedData
}

func FetchAndParseData() *SDN {
	res, err := util.GetContent("https://www.treasury.gov/ofac/downloads/sdn.xml", nil)
	if util.IsError(err) {
		return nil
	}
	defer res.Body.Close()

	xmlBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
		return nil
	}

	var data SDN
	err = xml.Unmarshal(xmlBytes, &data)
	if util.IsError(err) {
		return nil
	}

	return &data
}
