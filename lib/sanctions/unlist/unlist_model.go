package unlist

import (
	"encoding/xml"
)

//UnitedNationData defines the UN listing xml structure
type UnitedNationData struct {
	XMLName     xml.Name                 `xml:"CONSOLIDATED_LIST"`
	Individuals []UnitedNationIndividual `xml:"INDIVIDUALS>INDIVIDUAL"`
	Entities    []UnitedNationEntity     `xml:"ENTITIES>ENTITY"`
}

//Document defines  an identificaiton document
type Document struct {
	Type           string `xml:"TYPE_OF_DOCUMENT" json:"type"`
	Number         string `xml:"NUMBER" json:"number"`
	Note           string `xml:"NOTE" json:"note"`
	CountryOfIssue string `xml:"COUNTRY_OF_ISSUE" json:"country_of_issue"`
}

//Address defines an address
type Address struct {
	Street   string `xml:"STREET" json:"street"`
	City     string `xml:"CITY" json:"city"`
	Province string `xml:"STATE_PROVINCE" json:"province"`
	Country  string `xml:"COUNTRY" json:"country"`
	Note     string `xml:"NOTE" json:"note"`
}

//DateOfBirth defines a individual's date of birth
type DateOfBirth struct {
	Date  string `xml:"DATE" json:"date"`
	Day   string `xml:"DAY" json:"day"`
	Month string `xml:"MONTH" json:"month"`
	Year  string `xml:"YEAR" json:"year"`
}

//Alias defines an enity or indviduals alias
type Alias struct {
	Quality string `xml:"QUALITY" json:"quality"`
	Name    string `xml:"ALIAS_NAME" json:"alias_name"`
	Note    string `xml:"NOTE" json:"note"`
}

//UnitedNationIndividual defines an individual on the UN list
type UnitedNationIndividual struct {
	DataID       string       `xml:"DATAID" json:"data_id"`
	RefNo        string       `xml:"REFERENCE_NUMBER" json:"ref_no"`
	Titles       []string     `xml:"TITLE>VALUE" json:"titles"`
	ListTypes    []string     `xml:"LIST_TYPE>VALUE" json:"list_types"`
	UNListType   string       `xml:"UN_LIST_TYPE" json:"un_list_type"`
	FirstName    string       `xml:"FIRST_NAME" json:"first_name"`
	MiddleName   string       `xml:"THIRD_NAME" json:"middle_name"`
	LastName     string       `xml:"SECOND_NAME" json:"last_name"`
	Aliases      []Alias      `xml:"INDIVIDUAL_ALIAS" json:"aliases"`
	DateListed   string       `xml:"LISTED_ON" json:"date_listed"`
	Comments     string       `xml:"COMMENTS1" json:"comments"`
	Designations []string     `xml:"DESIGNATION>VALUE" json:"designations"`
	Nationality  string       `xml:"NATIONALITY>VALUE" json:"nationality"`
	DateOfBirth  *DateOfBirth `xml:"INDIVIDUAL_DATE_OF_BIRTH" json:"date_of_birth"`
	Documents    []Document   `xml:"INDIVIDUAL_DOCUMENT" json:"documents"`
	PlaceOfBirth *Address     `xml:"INDIVIDUAL_PLACE_OF_BIRTH" json:"place_of_birth"`
	Address      *Address     `xml:"INDIVIDUAL_ADDRESS" json:"address"`
}

//UnitedNationEntity defines an entity on the UN list
type UnitedNationEntity struct {
	DataID       string    `xml:"DATAID" json:"data_id"`
	RefNo        string    `xml:"REFERENCE_NUMBER" json:"ref_no"`
	Titles       []string  `xml:"TITLE>VALUE" json:"titles"`
	ListTypes    []string  `xml:"LIST_TYPE>VALUE" json:"list_types"`
	UNListType   string    `xml:"UN_LIST_TYPE" json:"un_list_type"`
	Name         string    `xml:"FIRST_NAME" json:"name"`
	Aliases      []Alias   `xml:"ENTITY_ALIAS" json:"aliases"`
	DateListed   string    `xml:"LISTED_ON" json:"date_listed"`
	Comments     string    `xml:"COMMENTS1" json:"comments"`
	Designations []string  `xml:"DESIGNATION>VALUE" json:"designations"`
	Nationality  string    `xml:"NATIONALITY>VALUE" json:"nationality"`
	Addresses    []Address `xml:"ENTITY_ADDRESS" json:"addresses"`
}
