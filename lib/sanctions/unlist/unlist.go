package unlist

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/util"
)

func ScrapeData() []model.FormattedData {
	var formattedData []model.FormattedData

	parsedData := Parse()
	if parsedData == nil {
		log.Println("[error] unable to fetch and parse UN CONSOLLIDATED SANCTIONS documents")
		return nil
	}

	for _, data := range parsedData.Individuals {

		metaData, err := util.JSONToMap(data)
		if util.IsError(err) {
			return nil
		}

		var aliases string
		for _, a := range data.Aliases {
			aliases += a.Name + " "
		}

		fd := model.FormattedData{
			Name:        data.FirstName + " " + data.MiddleName + " " + data.LastName,
			SourceName:  "UN CONSOLLIDATED SANCTIONS",
			SourceCode:  "UNCS",
			DataSet:     "sanction",
			Aliases:     aliases,
			Type:        model.INDIVIDUAL,
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			MetaData:    metaData,
		}

		formattedData = append(formattedData, fd)
	}

	for _, data := range parsedData.Entities {
		metaData, err := util.JSONToMap(data)
		if util.IsError(err) {
			return nil
		}

		var aliases string
		for _, a := range data.Aliases {
			aliases += a.Name + " "
		}

		fd := model.FormattedData{
			Name:        data.Name,
			SourceName:  "UN CONSOLLIDATED SANCTIONS",
			SourceCode:  "UNCS",
			Aliases:     aliases,
			Type:        model.ENTITY,
			LastUpdated: time.Now().UTC().Format("1994-01-02"),
			MetaData:    metaData,
		}

		for _, address := range data.Addresses {
			fd.Address += address.Street + ", " + address.City + ", " + address.Province + ", " + address.Country
		}

		formattedData = append(formattedData, fd)
	}
	return formattedData
}

func Parse() *UnitedNationData {
	res, err := util.GetContent("https://scsanctions.un.org/resources/xml/en/consolidated.xml", nil)
	if util.IsError(err) {
		return nil
	}
	defer res.Body.Close()

	xmlBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
		return nil
	}
	var unData UnitedNationData

	err = xml.Unmarshal(xmlBytes, &unData)
	if util.IsError(err) {
		return nil
	}

	return &unData
}
