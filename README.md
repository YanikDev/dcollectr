# Table of Contents

* [About Watchlist Central](#about)

* [Getting Started](#getting-started)

    * [Prerequisite](#prerequisite)

* [Usage](#usage)

    * [Running Web Scraper](#running-scraper)

    * [Starting API](#starting-api)

* [Installation](#installation)

* [Contribution](#contribution)

* [Developer](#developer)


# <a id="about"></a> About Watchlist Central

A REST-api and web scraper that provides single and bulk search functionalities in order to help financial institutions to be compliant with international standards. 


# <a id="getting-started"></a> Getting Started

Before proceeding to run the project ensure that the `config.yaml` file is configured to your environment.

## <a id="prerequisite"></a> Prerequisite
Before running the project make you have the following installed.

1. Golang v1.10+ [Download](https://golang.org/dl/) & [Installation Instructions](https://golang.org/doc/install)

2. Elastic Search v7.10+ [Download here](https://www.elastic.co/downloads/elasticsearch)

3. MongoDB v4.4+ [Download here](https://www.mongodb.com/try/download/community)

# <a id="usage"></a> Usage

### <a id="running-scraper"></a> Running web scrapper

1. Navigate into project directory via your terminal. for example: `cd ~/Documents/watchlist`

2. Enter the command: `go run main.go -start=scraper`

### <a id="starting-api"></a> Starting API Server

1. From project directory enter the command: `go run main.go -start=api`

# <a id="installation"></a> Installation

1. Build project on your local machine using the following command from the project root directory.
    
    Linux 64bit:

    ```
    GOOS=linux GOARCH=amd64 go build -o watchlistc main.go
    ```    

    Window 64bit:

    ```
    setx GOOS=windows 
    setx GOARCH=amd64 
    go build -o watchlistc main.go
    ```

    Mac 64bit:

    ```
    GOOS=darwin GOARCH=amd64 go build -o watchlistc main.go
    ```
2. Copy binary to server using SFTP Client or the following command

    ```
    scp jobs  root@server.com:/path/to/where/you/want/the/binary/to/execute/from
    ```
1. Create a Unit File in the `systemd` directory.

    ```sh
    sudo nano /etc/systemd/system/watchlistc.service
    ```
2. Add the following content to the `watchlistc.service` file.

    ```
    [Unit]
    Description=A REST-api that provides single and bulk search functionalities in order to help financial institutions to be compliant with international standards.
    After=network.target

    [Service]
    Type=simple
    User=ubuntu
    Restart=on-failure
    RestartSec=5s
    WorkingDirectory=/home/ubuntu/apps/{project-directory}/
    ExecStart=/home/ubuntu/apps/{project-directory}/watchlistc -start=api

    [Install]
    WantedBy=multi-user.target

    ```

3. Enable and start service
    ```
    sudo systemctl enable watchlistc
    sudo systemctl start watchlistc  
    ```

3. To start the web scraper start the application using the following command.
    ```
    sudo ./watchlistc -start=scraper 
    ```
    Start watchlist scraper using a cronjob to get periodic updates.

# <a id="contribution"></a> Project Contributions

Want to contribute to the project? Just follow the [go style guide](https://golang.org/doc/effective_go.html).

```
projectDir
 ┣ docs
 ┣ cmd
 ┃ ┣ api
 ┃ ┃ ┣ handler
 ┃ ┃ ┃ ┣ handler.go
 ┃ ┃ ┃ ┣ user_handler.go
 ┃ ┃ ┃ ┗ watchlist_handler.go
 ┃ ┃ ┣ router
 ┃ ┃ ┃ ┗ route.go
 ┃ ┃ ┗ api_server.go
 ┃ ┣ cli
 ┃ ┃ ┗ web_scraper.go
 ┣ lib
 ┃ ┣ criminals
 ┃ ┃ ┗ fbi
 ┃ ┃ ┃ ┣ fbi.go
 ┃ ┃ ┃ ┗ fbi_model.go
 ┃ ┣ peps
 ┃ ┃ ┗ jmgov
 ┃ ┃ ┃ ┗ jmgov.go
 ┃ ┣ sanctions
 ┃ ┃ ┣ bis
 ┃ ┃ ┃ ┣ bis.go
 ┃ ┃ ┃ ┗ bis_model.go
 ┃ ┃ ┣ sdn
 ┃ ┃ ┃ ┣ sdn.go
 ┃ ┃ ┃ ┗ sdn_model.go
 ┃ ┃ ┣ unlist
 ┃ ┃ ┃ ┣ unlist.go
 ┃ ┃ ┃ ┗ unlist_model.go
 ┃ ┃ ┗ wrldbk
 ┃ ┃ ┃ ┣ wrldbk.go
 ┃ ┃ ┃ ┗ wrldbk_model.go
 ┃ ┗ scraper.go
 ┣ logs
 ┃ ┣ .gitkeep
 ┃ ┗ watchlist.log
 ┣ model
 ┃ ┣ data_format_model.go
 ┃ ┗ user_model.go
 ┣ repository
 ┃ ┣ list
 ┃ ┃ ┣ watchlist_elasticsearch.go
 ┃ ┃ ┗ watchlist_mongo.go
 ┃ ┣ user
 ┃ ┃ ┗ user_repo.go
 ┃ ┗ repository.go
 ┣ util
 ┃ ┣ config.go
 ┃ ┣ db.go
 ┃ ┣ email.go
 ┃ ┣ errors.go
 ┃ ┣ format.go
 ┃ ┣ http.go
 ┃ ┣ logger.go
 ┃ ┗ validator.go
 ┣ .gitignore
 ┣ config.yaml
 ┣ go.mod
 ┣ go.sum
 ┣ main.go
 ┗ README.md
```
# <a id="developer"></a> Developer

Have any questions, concerns or recommendations please feel to contact me.

* Yanik Black <yanik.blake@technosoftja.com>