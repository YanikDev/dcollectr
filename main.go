package main

import (
	"flag"
	"os"
	"os/signal"

	"github.com/devprojx/dcollectr/cmd/api"
	"github.com/devprojx/dcollectr/cmd/cli"
	"github.com/devprojx/dcollectr/util"
)

func main() {
	var processFlag string
	var configPath string
	// flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.StringVar(&processFlag, "start", "api", "indicates whether to start the api server or the webscraper")
	flag.StringVar(&configPath, "config", "./config.yaml", "config file")
	flag.Parse()

	logger := util.NewLogger()

	logger.Info("initializing configurations...")
	cfg := util.Config{
		Logger: logger,
	}
	cfg.Load(configPath)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	switch processFlag {
	case "api":
		api.StartAPIServer(&cfg)
		break
	case "scraper":
		cli.StartWebScrapingProcess(&cfg)
		break
	}

	<-c

	os.Exit(0)

}
