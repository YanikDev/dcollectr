package repository

import (
	"context"

	"github.com/devprojx/dcollectr/model"
)

type ListRepo interface {
	Insert(ctx context.Context, records []interface{})
	Delete(ctx context.Context, m model.FormattedData)
	SingleSearch(ctx context.Context, req model.SearchRequest) []model.FormattedData
	BulkSearch(ctx context.Context, req []model.SearchRequest) map[string]model.BatchFormattedData
}

type UserRepo interface {
	InsertUser(ctx context.Context, record interface{}) interface{}
	UpdateAccount(ctx context.Context, email string, newPwd string) interface{}
	Delete(ctx context.Context, m model.FormattedData)
	FindUserByEmail(ctx context.Context, email string) *model.User
}
