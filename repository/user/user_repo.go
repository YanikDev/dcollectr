package user

import (
	"context"
	"log"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/repository"
)

func NewMongoUserRepo(conn *mongo.Client) repository.UserRepo {
	return &mongoUserRepo{
		Client: conn,
	}
}

type mongoUserRepo struct {
	Client *mongo.Client
}

func (r mongoUserRepo) getCollection(collectionName string) *mongo.Collection {
	db := r.Client.Database("dcollectr")
	collection := db.Collection(collectionName)
	return collection
}

func (r mongoUserRepo) InsertUser(ctx context.Context, record interface{}) interface{} {
	listings := r.getCollection("users")
	insertedResult, err := listings.InsertOne(ctx, record)
	if err != nil {
		log.Fatal(err)
	}

	return insertedResult.InsertedID
}

func (r mongoUserRepo) UpdateAccount(ctx context.Context, email string, newPwd string) interface{} {
	users := r.getCollection("users")
	filter := bson.M{"email": bson.M{"$eq": email}}
	update := bson.D{
		{"$inc", bson.D{
			{"password", newPwd},
		}},
	}

	updateResult, err := users.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}
	return updateResult.UpsertedID
}

func (r mongoUserRepo) UpdatePassword(ctx context.Context, email string, newPwd string) interface{} {
	users := r.getCollection("users")
	filter := bson.M{"email": bson.M{"$eq": email}}
	update := bson.D{
		{"$inc", bson.D{
			{"password", newPwd},
		}},
	}

	updateResult, err := users.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}
	return updateResult.UpsertedID
}

func (r mongoUserRepo) Delete(ctx context.Context, m model.FormattedData) {
	listings := r.getCollection("users")

	deleteResult, err := listings.DeleteOne(ctx, bson.D{
		{"email", m.SourceCode},
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("[info]: deleted " + m.SourceName + " documents  totalling - " + strconv.FormatInt(deleteResult.DeletedCount, 10))
}

func (r mongoUserRepo) FindUserByEmail(ctx context.Context, email string) *model.User {
	var searchFilters bson.M = make(primitive.M)

	listings := r.getCollection("users")

	searchFilters["email"] = bson.M{"$eq": email}

	var result *model.User
	err := listings.FindOne(ctx, searchFilters).Decode(result)
	if err != nil {
		log.Fatal("[critical error]: ", err)
	}

	return result
}
