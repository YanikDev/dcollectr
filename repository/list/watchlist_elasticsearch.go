package list

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/repository"
	"github.com/sirupsen/logrus"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esutil"
	"github.com/tidwall/gjson"
)

//NewElasticSearchRepo func
func NewElasticSearchRepo(conn *elasticsearch.Client, logger *logrus.Logger) repository.ListRepo {
	return &elasticListRepo{
		Client: conn,
		Logger: logger,
	}
}

type elasticListRepo struct {
	Client *elasticsearch.Client
	Logger *logrus.Logger
}

func (r elasticListRepo) Insert(ctx context.Context, records []interface{}) {

	length := len(records)
	if length == 0 {
		return
	}

	var countSuccessful uint64
	var x interface{} = records[0]
	rec := x.(model.FormattedData)

	r.Delete(ctx, rec)

	bulkRequest, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		Index:      "listings", // The default index name
		Client:     r.Client,   // The Elasticsearch client
		NumWorkers: length,     // The number of worker goroutines
		// FlushBytes:    int(flushBytes),  // The flush threshold in bytes
		FlushInterval: 30 * time.Second, // The periodic flush interval
	})

	for index, record := range records {
		id := strconv.FormatInt(int64(index), 10) + rec.SourceCode
		data, err := json.Marshal(&record)
		if err != nil {

		}

		err = bulkRequest.Add(
			context.Background(),
			esutil.BulkIndexerItem{
				Action:     "index",
				DocumentID: id,
				Body:       bytes.NewReader(data),
				OnSuccess: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
					atomic.AddUint64(&countSuccessful, 1)
				},
				OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
					if err != nil {
						log.Printf("[error]: fail to insert document: %s", err)
					} else {
						log.Printf("[error]: fail to insert document- %s: %s", res.Error.Type, res.Error.Reason)
					}
				},
			},
		)

		if err != nil {
			log.Fatalf("[error] unexpected error: %s", err)
		}
	}

	bulkRequestStats := bulkRequest.Stats()
	if err := bulkRequest.Close(context.Background()); err != nil {
		log.Fatalf("[error] unexpected error: %s", err)
	}
	if err != nil {
		log.Printf("[error] creating bulkRequest: %s", err)
	}

	if bulkRequestStats.NumFailed != 0 {
		fmt.Printf("[error]: document failed bulk insert: %d", bulkRequestStats.NumFailed)
	}

	log.Println("[info] inserted "+rec.SourceName+" documents totalling - ", countSuccessful)
}

func (r elasticListRepo) Delete(ctx context.Context, m model.FormattedData) {
	res, err := r.Client.DeleteByQuery(
		[]string{"listings"},
		strings.NewReader(fmt.Sprintf(`{
			"query": {
				"match": {
					"source_code": "%s"
				}
			}
		}`, m.SourceCode)))
	if err != nil {
		log.Println("[error] unable to delete "+m.SourceName+":  ", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		if res.StatusCode != 404 {
			log.Fatalf("[error]: unable to delete "+m.SourceName+": %s", res.String())
		}
	}

	var resDecode map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&resDecode); err != nil {
		log.Fatalf("[error] parsing the response body: %s", err)
	}

	log.Println("[info] deleted "+m.SourceName+" documents  totalling - ", resDecode["deleted"])
}

func (r elasticListRepo) SingleSearch(ctx context.Context, req model.SearchRequest) []model.FormattedData {
	_, results := r.searchDB(ctx, req)
	return results
}

func (r elasticListRepo) BulkSearch(ctx context.Context, req []model.SearchRequest) map[string]model.BatchFormattedData {
	var waitGroup sync.WaitGroup
	size := len(req)
	batchSize := 20
	endPoint := 0
	startingPoint := 0

	r.Logger.Info("request size: ", size)

	ch := make(chan struct {
		Index int
		Value string
	})

	start := time.Now()
	batchLength := int(math.Ceil(float64(size) / float64(batchSize)))
	for idx := 0; idx < batchLength; idx++ {
		startingPoint = endPoint
		endPoint = startingPoint + batchSize
		if endPoint > size {
			endPoint = size
		}

		waitGroup.Add(1)
		go func(index int, requests []model.SearchRequest) {
			defer waitGroup.Done()

			var finalQuery string
			for i := range requests {
				var filterQuery string

				//Build filter for source code eg. OFAC, WBP, UN, etc..
				if len(requests[i].Sources) != 0 {
					filterQuery += `{"match":  {"source_code":"` + strings.Join(requests[i].Sources, " ") + `"} },`
				}

				//Build filter for data sets eg. sanctions, peps, criminal
				if len(requests[i].DataSets) != 0 {
					filterQuery += `{"match":  {"data_set":"` + strings.Join(requests[i].DataSets, " ") + `"} },`
				}

				//Build filter for data types eg. Individual, Entity or Vessel
				if len(requests[i].Types) != 0 {
					filterQuery += `{"match":  {"type":"` + strings.Join(requests[i].Types, " ") + `"} },`
				}

				var nameFilter string
				if len(requests[i].Names) != 0 {
					searchValue := strings.Join(requests[i].Names, " ")
					nameFilter = `{ "multi_match": {`

					if requests[i].UseFuzzy {
						nameFilter += `"fuzziness": "1",`
						nameFilter += `"prefix_length": 1,`
					} else {
						nameFilter += `"fuzziness": "0.5",`
						nameFilter += `"prefix_length": 1,`
					}

					nameFilter += `"query": "` + searchValue + `",`
					nameFilter += `"operator": "and",`
					if requests[i].SearchAlias {
						nameFilter += `"fields": ["name", "aliases"]`
					} else {
						nameFilter += `"fields": ["name"]`
					}
					nameFilter += `}}`

					filterQuery += nameFilter
				}

				finalQuery += `{"index":"listings"}` + "\n" + `{"query": { "bool": { "must": [` + filterQuery + `] } } }` + "\r\n"

			}

			ch <- struct {
				Index int
				Value string
			}{index, finalQuery}
		}(idx, req[startingPoint:endPoint])
	}

	qch := make(chan []string)
	go func() {
		q := make([]string, batchLength)
		for c := range ch {
			q[c.Index] = c.Value
		}
		qch <- q
	}()

	waitGroup.Wait()
	close(ch)

	query := strings.Join(<-qch, "")
	close(qch)

	r.Logger.Info("time building query: ", time.Since(start))
	start = time.Now()
	res, err := r.Client.Msearch(strings.NewReader(query))
	if err != nil {
		log.Fatalf("[error] getting response from search query: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("[error] parsing the response body: %s", err)
		} else {
			log.Fatalf("[error] error from search - [%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	var b bytes.Buffer
	b.ReadFrom(res.Body)

	idx := 0
	batchData := make(map[string]model.BatchFormattedData)
	responses := gjson.GetManyBytes(b.Bytes(), "responses")
	responses[0].ForEach(func(index gjson.Result, values gjson.Result) bool {
		data := values.Map()
		var results []model.FormattedData
		data["hits"].Map()["hits"].ForEach(func(key gjson.Result, value gjson.Result) bool {
			var result model.FormattedData
			err := json.Unmarshal([]byte(value.Map()["_source"].String()), &result)
			if err != nil {
				log.Print("[error] error converting search results to json: ", err)
				return false
			}

			result.Score = value.Map()["_score"].Float()
			result.MaxScore = data["hits.max_score"].Float()
			results = append(results, result)
			return true
		})

		if len(results) != 0 {
			batchData[strings.Join(req[idx].Names, ",")] = model.BatchFormattedData{
				UniqueIdentifier: req[idx].UniqueIdentifier,
				Matches:          results,
			}
		}
		idx++
		return true
	})

	duration := time.Since(start)

	r.Logger.Info("time taken to complete search: "+duration.String()+" - total: ", len(batchData))

	return batchData
}

func (r *elasticListRepo) searchDB(ctx context.Context, req model.SearchRequest) (int64, []model.FormattedData) {
	var filters []map[string]interface{}

	//Build filter for source code eg. OFAC, WBP, UN, etc..
	if len(req.Sources) != 0 {
		filters = append(filters, map[string]interface{}{"match": map[string]string{"source_code": strings.Join(req.Sources, " ")}})
	}

	//Build filter for data sets eg. sanctions, peps, criminal
	if len(req.DataSets) != 0 {
		filters = append(filters, map[string]interface{}{"match": map[string]string{"data_set": strings.Join(req.DataSets, " ")}})
	}

	//Build filter for data types eg. Individual, Entity or Vessel
	if len(req.Types) != 0 {
		filters = append(filters, map[string]interface{}{"match": map[string]string{"type": strings.Join(req.Types, " ")}})
	}

	if len(req.Names) != 0 {
		match := make(map[string]interface{})
		searchValue := strings.Join(req.Names, " ")

		fields := []string{"name"}
		if req.SearchAlias {
			fields = append(fields, "aliases")
		}

		if req.UseFuzzy {
			match["fuzziness"] = "1"
			match["prefix_length"] = "1"
		} else {
			match["fuzziness"] = "0.5"
			match["prefix_length"] = "1"
		}

		match["query"] = searchValue
		match["fields"] = fields
		match["operator"] = "and"

		filters = append(filters, map[string]interface{}{"multi_match": match})
	}

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"bool": map[string]interface{}{
				"must": filters,
			},
		},
		"size": 10000,
	}

	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Fatalf("[error] encoding query: %s", err)
	}

	//Display raw query params
	// line, _ := buf.ReadString(byte('^'))
	// fmt.Println(line)

	res, err := r.Client.Search(
		r.Client.Search.WithContext(context.Background()),
		r.Client.Search.WithTrackTotalHits(true),
		r.Client.Search.WithIndex("listings"),
		r.Client.Search.WithBody(&buf),
		r.Client.Search.WithPretty(),
	)
	if err != nil {
		log.Fatalf("[error] getting response from search query: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("[error] parsing the response body: %s", err)
		} else {
			log.Fatalf("[error] error from search - [%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	var b bytes.Buffer
	b.ReadFrom(res.Body)

	values := gjson.GetManyBytes(
		b.Bytes(),
		"hits.total.value",
		"hits.max_score",
		"hits.hits",
	)

	var results []model.FormattedData
	values[2].ForEach(func(key gjson.Result, value gjson.Result) bool {
		var result model.FormattedData
		err := json.Unmarshal([]byte(value.Map()["_source"].String()), &result)
		if err != nil {
			log.Print("[error] error converting search results to json: ", err)
			return false
		}

		result.Score = value.Map()["_score"].Float()
		result.MaxScore = values[1].Float()
		results = append(results, result)
		return true
	})

	return values[0].Int(), results
}
