package list

import (
	"context"
	"log"
	"strconv"
	"strings"
	"sync"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/devprojx/dcollectr/model"
	"github.com/devprojx/dcollectr/repository"
)

func NewMongoListRepo(conn *mongo.Client) repository.ListRepo {
	return &mongoListRepo{
		Client: conn,
	}
}

type mongoListRepo struct {
	Client *mongo.Client
}

func (r mongoListRepo) getCollection() *mongo.Collection {
	db := r.Client.Database("dcollectr")
	collection := db.Collection("listings")
	return collection
}

//Insert bulk inserts a list of records
func (r mongoListRepo) Insert(ctx context.Context, records []interface{}) {
	length := len(records)
	if length == 0 {
		return
	}

	var x interface{} = records[0]
	record := x.(model.FormattedData)
	r.Delete(ctx, record)

	listings := r.getCollection()
	insertResult, err := listings.InsertMany(ctx, records)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("[info] inserted "+record.SourceName+" documents totalling - ", len(insertResult.InsertedIDs))
}

//Delete delete specific based on the source_code
func (r mongoListRepo) Delete(ctx context.Context, m model.FormattedData) {
	listings := r.getCollection()

	deleteResult, err := listings.DeleteMany(ctx, bson.D{
		{"source_code", m.SourceCode},
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("[info] deleted " + m.SourceName + " documents  totalling - " + strconv.FormatInt(deleteResult.DeletedCount, 10))
}

//SingleSearch performs a single search
func (r mongoListRepo) SingleSearch(ctx context.Context, req model.SearchRequest) []model.FormattedData {

	return r.buildSearchFilters(ctx, req)
}

//BulkSearch performs a bulk search
func (r mongoListRepo) BulkSearch(ctx context.Context, req []model.SearchRequest) map[string]model.BatchFormattedData {
	var wg sync.WaitGroup
	// size := len(req)
	ch := make(chan map[string]model.BatchFormattedData)

	listings := r.getCollection()
	for _, val := range req {
		//Remove single characters and empty names
		isEmpty := false
		for _, name := range val.Names {
			name := strings.Trim(name, " ")
			if name == "" || len(name) == 1 {
				isEmpty = true
				break
			}
		}
		if isEmpty {
			continue
		}

		wg.Add(1)
		searchParams := val
		go func() {
			r.searchDB(ctx, listings, &wg, ch, searchParams)
		}()
	}

	dch := make(chan map[string]model.BatchFormattedData)
	go func() {
		r := make(map[string]model.BatchFormattedData)
		for c := range ch {
			for key, value := range c {
				r[key] = value
			}
		}
		dch <- r
	}()

	wg.Wait()
	close(ch)

	data := <-dch
	close(dch)

	return data
}

//searchDB builds and execute search query
func (r *mongoListRepo) searchDB(ctx context.Context, listings *mongo.Collection, wg *sync.WaitGroup, ch chan map[string]model.BatchFormattedData, req model.SearchRequest) {
	defer func() {
		if a := recover(); a != nil {
			log.Println("RECOVER", a)
		}
		wg.Done()
	}()

	var results = r.buildSearchFilters(ctx, req)
	if len(results) == 0 {
		ch <- nil
		return
	}

	batchData := make(map[string]model.BatchFormattedData)
	batchData[strings.Join(req.Names, ",")] = model.BatchFormattedData{
		UniqueIdentifier: req.UniqueIdentifier,
		Matches:          results,
	}
	ch <- batchData
}

func (r *mongoListRepo) buildSearchFilters(ctx context.Context, req model.SearchRequest) []model.FormattedData {
	listings := r.getCollection()

	var searchFilters bson.M = make(primitive.M)
	var multipleNameFilters []bson.M

	for _, x := range req.Names {
		name := strings.Trim(x, " ")

		multipleNameFilters = append(multipleNameFilters, bson.M{
			"name": bson.M{"$in": []primitive.Regex{
				{Pattern: "\\b" + name + "\\b", Options: "i"},
			},
			},
		})
	}

	if req.SearchAlias == true {
		var multipleAliasesFilters []bson.M
		for _, x := range req.Names {

			multipleAliasesFilters = append(multipleAliasesFilters, bson.M{
				"aliases": bson.M{"$in": []primitive.Regex{
					{Pattern: x + ".*", Options: "im"},
				},
				},
			})
		}

		searchFilters["$and"] = multipleAliasesFilters
	}

	if len(multipleNameFilters) > 0 {
		searchFilters["$and"] = multipleNameFilters
	}

	if len(req.Sources) > 0 {
		searchFilters["source_code"] = bson.M{"$in": req.Sources}
	}
	if len(req.DataSets) > 0 {
		searchFilters["data_set"] = bson.M{"$in": req.DataSets}
	}
	if len(req.Types) > 0 {
		searchFilters["type"] = bson.M{"$in": req.Types}
	}

	opts := options.Find()
	opts.SetLimit(10)

	cur, err := listings.Find(ctx, searchFilters)

	if err != nil {
		log.Fatal("[critical error]: ", err)
	}

	var results []model.FormattedData
	cur.All(ctx, &results)

	return results
}
