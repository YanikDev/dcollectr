package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AccountStatus string

const (
	PAID       AccountStatus = "PAID"
	BLOCKED    AccountStatus = "BLOCKED"
	MISPAYMENT AccountStatus = "MISPAYMENT"
)

type User struct {
	ID              primitive.ObjectID `bson:"_id,omitempty"`
	CompanyName     string             `json:"company_name" xml:"company_name" bson:"company_name,omitempty" validate:"required,alphanumeric"`
	Email           string             `json:"name" xml:"name" bson:"name,omitempty" validate:"required,email"`
	Password        string             `json:"aliases" xml:"aliases" bson:"aliases,omitempty"`
	APIKeys         []APIKey           `json:"keys" xml:"keys" bson:"keys,omitempty"`
	AccountStatus   AccountStatus      `Json:"account_status" bson:"account_status"`
	NextPaymentDate time.Time          `json:"next_payment_date" xml:"next_payment_date" bson:"next_payment_date,omitempty"`
	SubscriptionID  primitive.ObjectID `json:"subscription_id" xml:"subscription_id" bson:"subscription_id,omitempty"`
	BillingAddress  *Address           `json:"billing_address" xml:"billing_address" bson:"billing_address" validate:"required,dive,required"`
}

type APIKey struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	Name       string             `json:"name" xml:"name" bson:"name,omitempty"`
	KeyHash    string             `json:"key_hash" xml:"key_hash" bson:"key_hash,omitempty"`
	Prefix     string             `json:"prefix" xml:"prefix" bson:"prefix,omitempty"`
	RateLimit  int                `json:"rate_limit" xml:"rate_limit" bson:"rate_limit,omitempty"`
	ValidSites []string           `json:"valid_sites" xml:"valid_sites" bson:"valid_sites,omitempty"`
}

type Subscription struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	Name      string             `json:"name" xml:"name" bson:"name,omitempty"`
	RateLimit string             `json:"rate_limit" xml:"rate_limit" bson:"rate_limit,omitempty"`
}

type Address struct {
	Street   string `json:"street" xml:"street" bson:"street" validate:"required,alphanumeric"`
	City     string `json:"city" xml:"city" bson:"city" validate:"required,alphanumeric"`
	Province string `json:"province" xml:"province" bson:"province" validate:"required,alpha"`
	Country  string `json:"country" xml:"country"  bson:"country" validate:"required,alpha"`
}
