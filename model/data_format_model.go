package model

type FormattedData struct {
	ID          string                 `bson:"_id,omitempty" json:"id,omitempty" xml:"id,omitempty"`
	Name        string                 `json:"name" xml:"name" bson:"name,omitempty"`
	Aliases     string                 `json:"aliases" xml:"aliases" bson:"aliases,omitempty"`
	Address     string                 `json:"address" xml:"address" bson:"address,omitempty"`
	SourceName  string                 `json:"source_name" xml:"source_name" bson:"source_name,omitempty"`
	SourceCode  string                 `json:"source_code" xml:"source_code" bson:"source_code,omitempty"`
	Type        Type                   `json:"type" xml:"type" bson:"type,omitempty"`
	DataSet     string                 `json:"data_set" xml:"data_set" bson:"data_set,omitempty"`
	LastUpdated string                 `json:"last_updated" xml:"last_updated" bson:"last_updated,omitempty"`
	Score       float64                `json:"score" xml:"score" bson:"score,omitempty"`
	MaxScore    float64                `json:"max_score" xml:"max_score" bson:"max_score,omitempty"`
	MetaData    map[string]interface{} `json:"meta_data" xml:"meta_data" bson:"meta_data,omitempty"`
}

type Type string

const (
	INDIVIDUAL Type = "Individual"
	ENTITY     Type = "Entity"
	VESSEL     Type = "Vessel"
	UNKNOWN    Type = "UnKnown"
)

type BatchFormattedData struct {
	UniqueIdentifier string          `json:"unique_identifier`
	Matches          []FormattedData `json:"matches"`
}

type SearchRequest struct {
	Names            []string `json:"names"`
	UniqueIdentifier string   `json:"unique_identifier"`
	DataSets         []string `json:"data_sets"`
	Sources          []string `json:"sources"`
	SearchAlias      bool     `json:"search_alias"`
	Types            []string `json:"types"`
	UseFuzzy         bool     `json:"use_fuzzy"`
}
