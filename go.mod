module github.com/devprojx/dcollectr

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/elastic/go-elasticsearch v0.0.0 // indirect
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20201104130540-2e1f801663c6
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gofiber/fiber/v2 v2.2.2
	github.com/google/uuid v1.1.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/klauspost/compress v1.11.3 // indirect
	github.com/lanziliang/logrus-rollingfile-hook v0.0.0-20181010120248-c944a72bb4e4
	github.com/lestrrat/go-file-rotatelogs v0.0.0-20180223000712-d3151e2a480f
	github.com/lestrrat/go-strftime v0.0.0-20180220042222-ba3bf9c1d042 // indirect
	github.com/mitchellh/mapstructure v1.4.0
	github.com/sirupsen/logrus v1.4.2
	github.com/tidwall/gjson v1.6.3
	go.mongodb.org/mongo-driver v1.4.4
	gopkg.in/yaml.v2 v2.4.0
)
