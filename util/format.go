package util

import (
	"bytes"
	"encoding/json"
	"html/template"
)

//JSONToMap converts a struct to a json then to a map data type
func JSONToMap(v interface{}) (map[string]interface{}, error) {
	jsonString, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	data := make(map[string]interface{})
	err = json.Unmarshal(jsonString, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

//HTMLTemplateToString converts a HTML Template to string
func HTMLTemplateToString(templateFileName string, data interface{}) (string, error) {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", err
	}

	return buf.String(), err
}
