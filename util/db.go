package util

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/cenkalti/backoff/v4"
	"github.com/elastic/go-elasticsearch/v7"
)

//InitMongoDB connects to an mongo database
func InitMongoDB(url string) (*mongo.Client, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(url))
	if err != nil {
		log.Printf("[error] creating mongo client: %s", err)
		return nil, err
	}
	ctx := context.TODO()
	err = client.Connect(ctx)
	if err != nil {
		log.Printf("[error] connecting to mongo db: %s", err)
		return nil, err
	}

	return client, nil
}

//InitElasticSearchDB connects to an elastic search database
func InitElasticSearchDB(urls []string, user string, password string) (*elasticsearch.Client, error) {
	addresses := []string{"localhost"}
	if len(urls) != 0 {
		addresses = urls
	}

	retryBackoff := backoff.NewExponentialBackOff()
	cfg := elasticsearch.Config{
		Addresses:     addresses,
		RetryOnStatus: []int{502, 503, 504, 429},
		RetryBackoff: func(i int) time.Duration {
			if i == 1 {
				retryBackoff.Reset()
			}
			return retryBackoff.NextBackOff()
		},
		MaxRetries: 5,
	}

	if user == "" && password == "" {
		cfg.Username = user
		cfg.Password = password
	}

	es, err := elasticsearch.NewClient(cfg)

	if err != nil {
		log.Fatalf("[error] creating elasticsearch client: %s", err)
		return nil, err
	}

	if _, err := es.Ping(); err != nil {
		log.Fatal("[error] unable to connect to elastic search server: ", err)
	}

	return es, nil
}
