package util

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/smtp"
	"strings"
)

func SendEmail(cfg Config, recipients []string, cc []string, subject string, message string, filename string, attachmentPath string) (bool, error) {
	delimeter := "**=myohmy689407924327"

	msg := fmt.Sprintf("From: %s\r\n", cfg.EmailClient.User)
	msg += fmt.Sprintf("To: %s\r\n", strings.Join(recipients, ";"))
	if len(cc) > 0 {
		msg += fmt.Sprintf("Cc: %s\r\n", strings.Join(cc, ";"))
	}
	msg += "Subject: " + subject + "\nMIME-Version: 1.0"
	msg += fmt.Sprintf("Content-Type: multipart/mixed; boundary=\"%s\"\r\n", delimeter)
	msg += "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\"\r\nContent-Transfer-Encoding: 7bit\r\n"
	msg += "\r\n" + message

	if attachmentPath != "" {
		msg += `Content-Type: text/plain; charset=utf-8
		Content-Transfer-Encoding: base64
		Content-Disposition: attachment;filename=\""` + filename

		rawFile, err := ioutil.ReadFile(attachmentPath)
		if err != nil {
			return false, err
		}
		msg += "\r\n" + base64.StdEncoding.EncodeToString(rawFile)
	}

	auth := smtp.PlainAuth("", cfg.EmailClient.User, cfg.EmailClient.Pwd, cfg.EmailClient.Host)
	if err := smtp.SendMail(cfg.EmailClient.Host+":"+cfg.EmailClient.Port, auth, cfg.EmailClient.User, recipients, []byte(msg)); err != nil {
		return false, err
	}

	return true, nil
}
