package util

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"time"
)

func GetContent(url string, headers []map[string]string) (*http.Response, error) {
	httpClient := &http.Client{
		Timeout: 120 * time.Second,
	}

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}

	request.Header.Set("User-Agent", "Not a browser")

	for _, m := range headers {
		for key, value := range m {
			request.Header.Set(key, value)
		}
	}

	res, err := httpClient.Do(request)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status error: %v", res.StatusCode)
	}

	return res, nil
}

//WriteHTTPResponse sends http response to the client
func WriteHTTPResponse(w http.ResponseWriter, status int, responseType string, data interface{}) {
	w.WriteHeader(status)
	if responseType == "xml" {
		w.Header().Set("Content-Type", "application/xml")
		xml.NewEncoder(w).Encode(data)
	} else {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(data)
	}
}
