package util

import (
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type ServerConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

//A Config defines the structure of the configuration file
type Config struct {
	Logger *logrus.Logger
	Server struct {
		AllowedOrigins []string     `yaml:"allowedOrigins"`
		API            ServerConfig `yaml:"api"`
		Web            ServerConfig `yaml:"web"`
	} `yaml:"server"`
	EmailClient struct {
		Host string `yaml:"host"`
		Port string `yaml:"port"`
		User string `yaml:"user"`
		Pwd  string `yaml:"pwd"`
	} `yaml:"email"`
	DB struct {
		Mongo struct {
			URI string `yaml:"uri"`
		} `yaml:"mongo"`
		ElasticSearch struct {
			URLS     []string `yaml:"urls"`
			Username string   `yaml:"username"`
			Password string   `yaml:"password"`
		} `yaml:"elastic"`
	} `yaml:"db"`
}

//Load loads configuration file (yaml) from a given path
func (c *Config) Load(path string) {
	//Open config file
	file, err := os.Open(path)
	if err != nil {
		c.Logger.Fatalf("unable to open config file: %s", err)
		return
	}
	defer file.Close()

	fileContent, err := ioutil.ReadAll(file)
	if err != nil {
		c.Logger.Fatalf("unable to read config file: %s", err)
		return
	}

	err = yaml.Unmarshal([]byte(fileContent), &c)
	if err != nil {
		c.Logger.Fatalf("unable to parse config file: %s", err)
	}
}
