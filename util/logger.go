package util

import (
	"os"

	rollingfile "github.com/lanziliang/logrus-rollingfile-hook"
	"github.com/sirupsen/logrus"
)

//NewLogger func
func NewLogger() *logrus.Logger {
	var log = logrus.New()
	log.Formatter = new(logrus.JSONFormatter)
	log.Formatter = new(logrus.TextFormatter)
	log.Formatter.(*logrus.TextFormatter).DisableColors = false
	log.Formatter.(*logrus.TextFormatter).DisableTimestamp = false
	log.Level = logrus.TraceLevel
	log.Out = os.Stdout

	if _, err := os.Stat("./logs"); err != nil {
		if os.IsNotExist(err) {
			err := os.Mkdir("./logs", 0755)
			if err != nil {
				log.Fatalf("failed to create log directory: %s", err)
			}
		}
	}

	hook, err := rollingfile.NewRollingFileTimeHook("./logs/watchlist.log", "2006-01-02", 5)
	if err != nil {
		log.Fatalf("unable to create log file: %s", err)
	}
	defer hook.Close()

	log.AddHook(hook)

	return log
}
