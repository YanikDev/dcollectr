package util

import (
	"log"
)

func IsError(err error) bool {
	if err != nil {
		log.Print("[error] ", err)
		return true
	}
	return false
}
